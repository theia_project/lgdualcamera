package tw.theia.lgdualcamera;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static String TAG = "HELLOCAMERA";
    private static final int PREVIEW_SIZE_WIDTH = 1920;
    private static final int PREVIEW_SIZE_HEIGHT = 1080;
    private static final int PICTURE_SIZE_WIDTH = 3840;
    private static final int PICTURE_SIZE_HEIGHT = 2160;

    private static final int PERMISSION_REQUEST_CAMERA = 1;
    private static final int PERMISSION_REQUEST_STORAGE = 2;

    private static final int CAMERA_MAX_NUM = 3;
    private int cameraNum;

    TextureView textureView1;
    TextureView textureView2;

    Camera camera1;
    Camera camera2;

    int cameraId1;
    int cameraId2;

    Camera.CameraInfo cameraInfo1 = new Camera.CameraInfo();
    Camera.CameraInfo cameraInfo2 = new Camera.CameraInfo();
    Camera.CameraInfo cameraInfo3 = new Camera.CameraInfo();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(camera1==null || camera2==null){
                    return;
                }
                camera1.takePicture(null, null, mPicture1);
                camera2.takePicture(null, null, mPicture2);
            }
        });

        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(camera1==null || camera2==null){
                    return;
                }
                StringBuilder sb = new StringBuilder();
                CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                try {
                    for(String id : new String[]{String.valueOf(cameraId1), String.valueOf(cameraId2)}){
                        sb.append("Camera ");
                        sb.append(id);
                        sb.append("\n");
                        sb.append("Characteristics:\n");
                        CameraCharacteristics characteristics = manager.getCameraCharacteristics(id);
                        for(CameraCharacteristics.Key key: characteristics.getKeys()){
                            sb.append(key.getName());
                            sb.append("\n");
                        }
                        sb.append("================\n\n");
                    }
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                builder.setMessage(sb.toString());
                builder.show();

                Log.e("INFO", sb.toString());
            }
        });

        // make sure to have camera permission
        if (!checkCameraPermission()) {
            requestCameraPermission();
        }

        // make sure to have storage permission
        if (!checkStoragePermission()) {
            requestStoragePermission();
        }

        if(checkCameraPermission()){
            initCamera();
        }
    }

    private void initCamera(){
        cameraNum = Camera.getNumberOfCameras(); // the number of available cameras
        Log.v(TAG, "cameraNum: " + cameraNum);
        if (!(cameraNum > 0) || !checkCameraFeature(this)) {
            // This device has no camera.
            Log.e(TAG, "no camera");
            return;
        }

        if (cameraNum >= CAMERA_MAX_NUM) {
            textureView1 = findViewById(R.id.camera_preview1);
            textureView2 = findViewById(R.id.camera_preview2);


            // identify dual camera IDs
            findDualCameraId();
        }
    }

    private TextureView.SurfaceTextureListener surfaceTextureListener1 = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            setUpCamera1();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };

    private TextureView.SurfaceTextureListener surfaceTextureListener2 = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            setUpCamera2();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };

    /**
     * Identifies dual camera IDs
     */
    private void findDualCameraId() {
        Camera.getCameraInfo(0, cameraInfo1);
        Camera.getCameraInfo(1, cameraInfo2);
        Camera.getCameraInfo(2, cameraInfo3);

        // examine which camera devices have the same facing direction
        if (cameraInfo1.facing == cameraInfo2.facing) {
            cameraId1 = 1;
            cameraId2 = 0;
        } else if (cameraInfo1.facing == cameraInfo3.facing) {
            cameraId1 = 2;
            cameraId2 = 0;
        } else {
            cameraId1 = 2;
            cameraId2 = 1;
        }

        Log.i(TAG, "1st dual camera ID: " + cameraId1);
        Log.i(TAG, "2nd dual camera ID: " + cameraId2);
    }

    /**
     * Checks whether the device has camera device
     *
     * @param context Context of the application
     * @return true if the device has camera feature, or false if not
     */
    private boolean checkCameraFeature(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks whether the application has CAMERA permission
     *
     * @return true if the application has CAMERA permission, or false if not
     */
    private boolean checkCameraPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "App doesn't have CAMERA Permission!!");
            return false;
        }
        Log.i(TAG, "App has CAMERA permission.");
        return true;
    }

    /**
     * Requests CAMERA permission
     */
    private void requestCameraPermission() {
        Log.i(TAG, "Requesting CAMERA permission");
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CAMERA);
    }

    /**
     * Checks whether the application has WRITE_EXTERNAL_STORAGE permission
     *
     * @return true if the application has WRITE_EXTERNAL_STORAGE permission, or false if not
     */
    private boolean checkStoragePermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "App doesn't have WRITE_EXTERNAL_STORAGE Permission!!");
            return false;
        }
        Log.i(TAG, "App has WRITE_EXTERNAL_STORAGE permission.");
        return true;
    }

    /**
     * Requests WRITE_EXTERNAL_STORAGE permission
     */
    private void requestStoragePermission() {
        Log.i(TAG, "Requesting WRITE_EXTERNAL_STORAGE permission");
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "requestCode: " + requestCode);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        int nIdx = 0;
        for (int grantResult : grantResults) {
            if (requestCode == PERMISSION_REQUEST_CAMERA &&
                    grantResult == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "permission granted: " + permissions[nIdx]);
                initCamera();
            }
            if (requestCode == PERMISSION_REQUEST_STORAGE &&
                    grantResult == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "permission granted: " + permissions[nIdx]);
            }
            nIdx++;
        }
    }

    /**
     * Called when image data is available after a picture is taken on 1st dual camera device
     */
    private Camera.PictureCallback mPicture1 = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            Log.i(TAG, "onPictureTaken on 1st dual camera");

            // create a file to save the picture taken
            File pictureFile = getOutputMediaFile(1);
            if (null == pictureFile){
                Log.e(TAG, "Error in creating media file. Check storage permissions.");
                return;
            }

            // write image data (captured image) to the file prepared
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.e(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.e(TAG, "Error accessing file: " + e.getMessage());
            }

            // toast notification that the image file has been saved
            CharSequence text = String.format(pictureFile.getName() + " saved");
            Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
            toast.show();

            // preview must be restarted after image capture (preview stops after image capture)
            camera.startPreview();

            // double check current size settings
            Camera.Size previewSize = camera.getParameters().getPreviewSize();
            Camera.Size pictureSize = camera.getParameters().getPictureSize();
            Log.v(TAG, "preview size: " + previewSize.width + "x" + previewSize.height);
            Log.v(TAG, "picture size: " + pictureSize.width + "x" + pictureSize.height);
        }
    };

    /**
     * Called when image data is available after a picture is taken on 2nd dual camera device
     */
    private Camera.PictureCallback mPicture2 = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            Log.i(TAG, "onPictureTaken on 2nd dual camera");

            // create a file to save the picture taken
            File pictureFile = getOutputMediaFile(2);
            if (null == pictureFile){
                Log.e(TAG, "Error in creating media file. Check storage permissions.");
                return;
            }

            // write image data (captured image) to the file prepared
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.e(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.e(TAG, "Error accessing file: " + e.getMessage());
            }

            // toast notification that the image file has been saved
            CharSequence text = String.format(pictureFile.getName() + " saved");
            Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
            toast.show();

            // preview must be restarted after image capture (preview stops after image capture)
            camera.startPreview();

            // double check current size settings
            Camera.Size previewSize = camera.getParameters().getPreviewSize();
            Camera.Size pictureSize = camera.getParameters().getPictureSize();
            Log.v(TAG, "preview size: " + previewSize.width + "x" + previewSize.height);
            Log.v(TAG, "picture size: " + pictureSize.width + "x" + pictureSize.height);
        }
    };

    /**
     * Creates a file to save image data
     *
     * @return image file
     */
    private static File getOutputMediaFile(int cameraId){
        // create a directory to hold saved files
        // directory location: Pictures/MyCameraApp
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.i(TAG, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "CAM"+cameraId+"_"+ timeStamp + ".jpg");

        return mediaFile;
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();

        if(textureView1.isAvailable()) {
            // open camera 1
            setUpCamera1();
        }
        else {
            textureView1.setSurfaceTextureListener(surfaceTextureListener1);
        }

        if(textureView2.isAvailable()) {
            // open camera 2
            setUpCamera2();
        }
        else {
            textureView2.setSurfaceTextureListener(surfaceTextureListener2);
        }
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");

        if (null != camera2) {
            Log.i(TAG, "onPause close cam: " + cameraId2);
            camera2.stopPreview();
            camera2.release();
            camera2 = null;
        }

        if (null != camera1) {
            Log.i(TAG, "onPause close cam: " + cameraId1);
            camera1.stopPreview();
            camera1.release();
            camera1 = null;
        }

        super.onPause();
    }

    /**
     * Opens the first dual camera and displays its preview
     */
    private void setUpCamera1() {
        camera1 = openCamera(cameraId1);
        setPreviewSize(camera1, cameraId1);
        setOrientation(camera1, cameraId1);
        startPreview(camera1, cameraId1, textureView1.getSurfaceTexture());
    }

    /**
     * Opens the second dual camera and displays its preview
     */
    private void setUpCamera2() {
        camera2 = openCamera(cameraId2);
        setPreviewSize(camera2, cameraId2);
        setOrientation(camera2, cameraId2);
        startPreview(camera2, cameraId2, textureView2.getSurfaceTexture());
    }

    /**
     * Opens the camera with the specified camera ID
     *
     * @param cameraId camera ID
     * @return Camera object
     */
    private Camera openCamera(int cameraId) {
        Camera camera = null;
        try {
            camera = Camera.open(cameraId);
        }
        catch (Exception e) {
            Log.e(TAG, "error when opening camera " + cameraId);
            e.printStackTrace();
        }
        return camera;
    }

    /**
     * Sets the preview size and the picture size on a camera device
     *
     * @param camera Camera object
     * @param cameraId camera ID
     */
    private void setPreviewSize(Camera camera, int cameraId) {
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size previewSize = camera.new Size(PREVIEW_SIZE_WIDTH, PREVIEW_SIZE_HEIGHT);
        Camera.Size pictureSize = camera.new Size(PICTURE_SIZE_WIDTH, PICTURE_SIZE_HEIGHT);

        // set preview size
        List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
        for(Camera.Size size : previewSizes) {
            Log.v(TAG, "Camera " + cameraId + " preview supported: " + size.width + "x" + size.height);
        }

        if(previewSizes.contains(previewSize)) {
            // if the chosen size is supported
            Log.i(TAG, "Camera " + cameraId + " preview size:" + previewSize.width + "x" + previewSize.height);
            parameters.setPreviewSize(previewSize.width, previewSize.height);
        }
        else {
            // alternatively, use the maximum resolution supported
            Log.i(TAG, "Camera " + cameraId + " fallback preview size:" + previewSizes.get(0).width + "x" + previewSizes.get(0).height);
            parameters.setPictureSize(previewSizes.get(0).width, previewSizes.get(0).height);
        }

        // set picture size
        List<Camera.Size> pictureSizes = parameters.getSupportedPictureSizes();
        for(Camera.Size size : pictureSizes) {
            Log.v(TAG, "Camera " + cameraId + " picture supported: " + size.width + "x" + size.height);
        }

        if(pictureSizes.contains(pictureSize)) {
            // if the chosen size is supported
            Log.i(TAG, "Camera " + cameraId + " picture size:" + pictureSize.width + "x" + pictureSize.height);
            parameters.setPictureSize(pictureSize.width, pictureSize.height);
        }
        else {
            // alternatively, use the maximum resolution supported
            Log.i(TAG, "Camera " + cameraId + " fallback picture size:" + pictureSizes.get(0).width + "x" + pictureSizes.get(0).height);
            parameters.setPictureSize(pictureSizes.get(0).width, pictureSizes.get(0).height);
        }

        camera.setParameters(parameters);
    }

    /**
     * Sets the orientation of the preview frame of a camera device
     *
     * @param camera Camera object
     * @param cameraId camera ID
     */
    private void setOrientation(Camera camera, int cameraId) {
        // adjust camera orientation
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, cameraInfo);
        int orientation = cameraInfo.orientation;
        Log.v(TAG, "orientation of camera " + cameraId + ": " + orientation);

        camera.setDisplayOrientation(90);
    }

    /**
     * Starts displaying preview from a camera
     *
     * @param camera Camera object
     * @param cameraId camera ID
     * @param surface SurfaceTexture of the TextureView on which the preview is displayed
     */
    private void startPreview(Camera camera, int cameraId, SurfaceTexture surface) {
        try {
            if(null != camera) {
                Log.i(TAG, "starting preview on camera ID: " + cameraId);
                camera.setPreviewTexture(surface);
                camera.startPreview();
            }
        } catch (IOException e) {
            Log.e(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

}